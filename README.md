# GitLab Docs project (Legacy)

This repository contains the original GitLab documentation website that served `docs.gitlab.com` from December 2016 to February 2025. The site was built using [Nanoc](https://nanoc.app/).

## Important notice: docs website has moved

The GitLab Docs website codebase has moved to a new location:
https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com

### For documentation issues

- For issues related to the docs website, please create them at:
  https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues
- For documentation content issues, please refer to:
  https://docs.gitlab.com/development/documentation/

## Additional information

- [LICENSE](LICENSE): MIT License

Note: The Docker images in this repository remain available as they contain version-specific documentation that matches past GitLab releases.
